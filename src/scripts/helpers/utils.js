// eslint-disable-next-line no-console
export const { warn, error } = console;

export const debounce = (cb, interval = 0) => {
    let debounceTimeoutId;
    return function (...args) {
        clearTimeout(debounceTimeoutId);
        debounceTimeoutId = null;
        debounceTimeoutId = setTimeout(() => cb.apply(this, args), interval);
    };
};

export const throttle = (cb, delay) => {
    let lastCall = 0;
    return function (...args) {
        const now = new Date().getTime();
        if (now - lastCall < delay) return;
        lastCall = now;
        return cb(...args);
    };
};

export const rAF = cb => {
    let globalID;
    let ticking = false;
    return function (...args) {
        if (!ticking) {
            cancelAnimationFrame(globalID);
            globalID = null;
            globalID = requestAnimationFrame(() => {
                ticking = false;
                return cb(...args);
            });
            ticking = true;
        }
    };
};

export const hiddenScroll = () => {
    const scrollTop = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
    document.documentElement.classList.add('no-scroll');
    document.documentElement.style.top = `${-scrollTop}px`;
};

export const visibleScroll = () => {
    const scrollTop = parseInt(document.documentElement.style.top);
    document.documentElement.classList.remove('no-scroll');
    document.documentElement.style.removeProperty('top');
    document.documentElement.scrollTop = -scrollTop;
    document.body.scrollTop = -scrollTop;
};

export const useIntersectionObserver = (childNode, cb, options = {}) => {
    const observer = new IntersectionObserver(([target], observerOptions) => {
        cb(target, observerOptions);
    }, options);

    observer.observe(childNode);

    return () => {
        observer.unobserve(childNode);
    };
};

export const wrapWords = str => str.replace(/\w+/g, '<span class="word">$&</span>');

export const wrapChars = str => str.replace(/\w/g, '<span class="letter">$&</span>');

export const popupShower = (popupName, method = false) => {
    const popup = document.getElementById(`popup-${popupName}`);
    if (method === 'show') {
        popup.removeAttribute('style');
        popup.classList.add('js-popup-opened');
        hiddenScroll();
    } else {
        popup.classList.remove('js-popup-opened');
        visibleScroll();
    }
};

export const setCurrentYear = $el => ($el.innerText = new Date().getFullYear());
