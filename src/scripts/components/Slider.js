import Swiper from 'swiper';
import SwiperCore, { Navigation } from 'swiper/core';

SwiperCore.use([Navigation]);

export default class Slider {
    constructor($slider) {
        this.$slider = $slider;
    }

    init() {
        const $slider = this.$slider;
        new Swiper($slider, {
            spaceBetween: 46,
            slidesPerView: 'auto',
            initialSlide: 0,
            watchOverflow: true,
            observer: true,
            observeParents: true,
            observeSlideChildren: true,
            navigation: {
                prevEl: $slider.closest('[data-slider-wrap]').querySelector('.swiper-button-prev'),
                nextEl: $slider.closest('[data-slider-wrap]').querySelector('.swiper-button-next'),
            },
        });
    }
}
