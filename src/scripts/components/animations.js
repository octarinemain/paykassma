import anime from 'animejs/lib/anime.es.js';
// import { playIllustration } from '@components/illustrations';

const removeFromElementsPropWillChange = elements => {
    elements.forEach(({ target }) => {
        target.classList.add('reset-will-change');
    });
};

export const startAnimations = () => {
    // {
    //     const $title = document.querySelector('#with-us-title');
    //     $title.innerHTML = $title.textContent.replace(/\S/g, '<span class="letter">$&</span>');
    //     anime({
    //         targets: $title,
    //         opacity: 1,
    //         duration: 0,
    //         complete({ animatables }) {
    //             removeFromElementsPropWillChange(animatables);
    //         },
    //     });
    //     anime({
    //         targets: '#with-us-title .letter',
    //         translateY: {
    //             value: [-75, 0],
    //             easing: 'easeOutElastic(1, .4)',
    //         },
    //         scale: [0, 1],
    //         duration: 1500,
    //         delay: (_, i) => 20 * i,
    //         complete({ animatables }) {
    //             removeFromElementsPropWillChange(animatables);
    //         },
    //     });
    // }
    // {
    //     const $header = document.querySelector('#header');
    //     anime({
    //         targets: $header,
    //         translateY: {
    //             value: ['-150%', '0%'],
    //             delay: 1000,
    //             duration: 500,
    //             easing: 'easeInOutSine',
    //         },
    //         complete({ animatables }) {
    //             removeFromElementsPropWillChange(animatables);
    //             playIllustration('with-us-illustration');
    //         },
    //     });
    // }
    // anime({
    //     targets: '#with-us-description',
    //     translateY: {
    //         value: [150, 0],
    //         delay: 500,
    //         duration: 1500,
    //         easing: 'easeInOutSine',
    //     },
    //     opacity: {
    //         value: 1,
    //         delay: 1500,
    //         duration: 400,
    //         easing: 'easeInOutSine',
    //     },
    //     complete({ animatables }) {
    //         removeFromElementsPropWillChange(animatables);
    //     },
    // });
    // anime({
    //     targets: ['#with-us-btn', '#with-us-btn-mob'],
    //     translateY: {
    //         value: [20, 0],
    //         delay: 1700,
    //         duration: 600,
    //         easing: 'easeInOutSine',
    //     },
    //     opacity: {
    //         value: 1,
    //         delay: 1700,
    //         duration: 400,
    //         easing: 'easeInOutSine',
    //     },
    //     complete({ animatables }) {
    //         removeFromElementsPropWillChange(animatables);
    //     },
    // });
    // anime({
    //     targets: '#with-us-benefits .card',
    //     translateY: {
    //         value: ['100%', 0],
    //     },
    //     opacity: {
    //         value: 1,
    //     },
    //     duration: (_, i) => 600 + i * 100,
    //     delay: (_, i) => 1700 + i * 100,
    //     easing: 'easeInOutSine',
    //     complete({ animatables }) {
    //         removeFromElementsPropWillChange(animatables);
    //     },
    // });
    // anime({
    //     targets: '#with-us-benefits .card__holder',
    //     translateY: {
    //         value: ['70%', 0],
    //     },
    //     opacity: {
    //         value: 1,
    //     },
    //     duration: (_, i) => 1100 - i * 100,
    //     delay: (_, i) => 2000 + i * 150,
    //     easing: 'easeInOutSine',
    //     complete({ animatables }) {
    //         removeFromElementsPropWillChange(animatables);
    //     },
    // });
};
