import axios from 'axios';
import { generateCancelRequest } from '@helpers/axios';
import { warn, popupShower } from '@helpers/utils';

export const sendMail = $form => {
    $form.addEventListener('submit', e => {
        e.preventDefault();
        const reqPost = { cancel: null };
        const { cancelToken, isCancel } = generateCancelRequest(reqPost);
        const formData = new FormData($form);

        (async () => {
            try {
                const response = await axios.post('email/sendmail.php', formData, {
                    cancelToken,
                });

                if (response) {
                    const { status, statusText } = response;
                    if (status === 200 || statusText === 'OK') {
                        popupShower('notification', 'show');
                        $form.reset();
                    }
                }

                reqPost.cancel = null;
            } catch (error) {
                if (isCancel(error)) {
                    return;
                }
                alert('Opps! Something wrong... ¯/_(ツ)_/¯');

                warn(error.message);
            }
        })();
    });
};

// data-popup-open data-popup="popup-notification"
