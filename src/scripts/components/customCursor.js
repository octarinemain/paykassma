export const customCursor = $holder => {
    const $cursor = $holder.querySelector('[data-crs]');
    const rect = $cursor.getBoundingClientRect();

    const offsetCursor = e => {
        if (
            e.pageY >= $holder.offsetTop &&
            e.pageY <= $holder.offsetTop + $holder.offsetHeight &&
            e.pageX >= $holder.offsetLeft &&
            e.pageX <= $holder.offsetLeft + $holder.offsetWidth
        ) {
            $cursor.setAttribute(
                'style',
                `top: ${e.pageY - $holder.offsetTop - rect.height / 2}px; left: ${
                    e.pageX - $holder.offsetLeft - rect.width / 2
                }px; opacity: 1; z-index: 5`
            );
        } else {
            $cursor.removeAttribute('style');
            $cursor.style.transition = 'none';
        }
    };

    document.addEventListener('mousemove', offsetCursor);

    const complete = e => {
        e.target.removeEventListener('animationend', complete);
        e.target.style.animation = 'none';
    };

    const handler = () => {
        $cursor.removeEventListener('animationend', complete);
        $cursor.style.animation = 'none';
        $cursor.style.animation = 'ease-out .3s clicked';
        $cursor.addEventListener('animationend', complete);
    };

    $holder.onclick = handler;
};
