import { useIntersectionObserver } from '@helpers/utils';
import {
    titleAnime,
    headerAnime,
    liftUpAnime,
    cardsStatAnime,
    cardsAdvantagesAnime,
    cardsAdvantagesAnimated,
    buttonAnime,
    fadeInAnime,
} from '@/scripts/components/animeDetectViewport/animeDetectViewport.function';

export default class AnimeDetectViewport {
    constructor($els) {
        this.$els = $els;
        this.observes = [];
    }

    init() {
        this.$els.forEach(($el, i) => {
            this.detectEl($el, i);
        });
    }

    detectEl($el, i) {
        this.observes.push(
            useIntersectionObserver($el, target => {
                if (target.isIntersecting) {
                    const value = $el.getAttribute('data-el-anime');

                    switch (value) {
                        case 'header':
                            headerAnime($el);
                            break;
                        case 'with-us-title':
                            titleAnime($el, 75);
                            break;
                        case 'with-us-description':
                            liftUpAnime($el, 150, 1500, 500, 'with-us');
                            break;
                        case 'with-us-statistics':
                            cardsStatAnime($el);
                            break;
                        case 'services-title-first':
                        case 'services-title-second':
                        case 'services-title-third':
                        case 'advantages-title':
                        case 'how-to-start-title':
                        case 'countries-title':
                            titleAnime($el, '65%');
                            break;
                        case 'services-description-first':
                        case 'services-description-second':
                        case 'services-description-third':
                            liftUpAnime($el, '60%', 500, 100);
                            break;
                        case 'with-us-illustration':
                        case 'services-illustration-first':
                        case 'services-illustration-second':
                        case 'services-illustration-third':
                            $el.getLottie().play();
                            break;
                        case 'advantages-cards':
                            cardsAdvantagesAnime($el);
                            break;
                        case 'how-to-start-steps':
                            cardsAdvantagesAnimated($el);
                            break;
                        case 'how-to-start-btn':
                        case 'countries-btn':
                            buttonAnime($el);
                            break;
                        case 'countries-pane':
                            liftUpAnime($el, '40%', 700, 100);
                            break;
                        case 'countries-map':
                            fadeInAnime($el, 500, 120);
                            break;
                        default:
                            break;
                    }
                    this.observes[i]();
                }
            })
        );
    }
}
