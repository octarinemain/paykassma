export default class NavbarMob {
    constructor($toggler, $nav) {
        this.$toggler = $toggler;
        this.$nav = $nav;
        this.$holder = this.$nav.closest('[data-nav-holder]');
        this.handler = this.handler.bind(this);
        this.transitionend = this.transitionend.bind(this);
    }

    init() {
        this.$toggler.addEventListener('click', this.handler);
    }

    handler(e) {
        const $target = e.target.closest('[data-navbar-toggler]');
        if ($target.classList.contains('is-active')) {
            $target.classList.remove('is-active');
            this.$nav.classList.remove('is-open');
            this.$holder.classList.remove('is-open');
            this.$nav.style.maxHeight = `${this.$nav.scrollHeight}px`;
            this.$nav.removeAttribute('style');
            return;
        }
        $target.classList.add('is-active');
        this.$holder.classList.add('is-open');
        this.$nav.style.maxHeight = `${this.$nav.scrollHeight}px`;
        this.$nav.addEventListener('transitionend', this.transitionend);
    }

    transitionend(e) {
        this.$nav.removeEventListener('transitionend', this.transitionend);
        if (e.target.style.maxHeight) {
            e.target.classList.add('is-open');
            return false;
        }
        e.target.classList.remove('is-open');
    }

    destroy() {
        this.$toggler.removeEventListener('click', this.handler);
        this.$nav.removeEventListener('transitionend', this.transitionend);
        this.$toggler.classList.remove('is-active');
        this.$nav.classList.remove('is-open');
        this.$nav.removeAttribute('style');
        this.$holder.classList.remove('is-open');
    }
}
