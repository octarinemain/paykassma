export default class NavbarDesktop {
    constructor($navbar) {
        this.$navbar = $navbar;
        this.handlerMouseover = this.handlerMouseover.bind(this);
    }

    init() {
        this.$navbar.addEventListener('mouseover', this.handlerMouseover);
        this.$navbar.addEventListener('mouseout', this.handlerMouseout);
    }

    handlerMouseover(e) {
        const $target = e.target.closest('[data-submenu-toggler]');

        if ($target && !$target.classList.contains('is-open')) {
            $target.classList.add('is-open');
            const $holder = $target.querySelector('[data-submenu-holder]');
            $holder.style.maxHeight = `${$holder.scrollHeight}px`;
        }
        return;
    }

    handlerMouseout(e) {
        const $target = e.target.closest('[data-submenu-toggler]');

        if ($target && $target.classList.contains('is-open')) {
            $target.classList.remove('is-open');
            $target.querySelector('[data-submenu-holder]').removeAttribute('style');
        }
        return;
    }

    destroy() {
        this.$navbar.removeEventListener('mouseover', this.handlerMouseover);
        this.$navbar.removeEventListener('mouseout', this.handlerMouseout);
    }
}
