import { Loader } from '@components/loader/Loader';
import { EventEmitter } from '@helpers/EventEmitter';
import Popups from '@components/Popups';
import Slider from '@components/Slider';
import AnimeDetectViewport from '@/scripts/components/animeDetectViewport/AnimeDetectViewport';
import { resizer } from '@helpers/resizer';
import { customCursor } from '@components/customCursor';
import Accordion from '@components/accordion/Accordion';
import { scrollTo } from '@helpers/animatedScrollTo';
import NavbarDesktop from '@/scripts/components/navbar/NavbarDesktop';
import NavbarMob from '@/scripts/components/navbar/NavbarMob';
import { sendMail } from '@components/sendMail';
import { setCurrentYear } from '@helpers/utils';

Loader.init(document.getElementById('loader'), document.getElementById('overlay'));

const emitter = new EventEmitter();

// NOTE: matchMedia for animation 'with-us-illustration'
{
    const $element = document.querySelector('[data-mq-el-anime]');
    const name = 'with-us-illustration';
    const mq = window.matchMedia('(max-width: 1199px)');
    const checkMq = () => {
        if (mq.matches) {
            $element.removeAttribute('id');
            $element.setAttribute('data-el-anime', name);
        } else {
            $element.removeAttribute('data-el-anime');
            $element.id = name;
        }
    };

    checkMq();
    mq.addListener(checkMq);
}

// Popups
{
    const $popups = document.querySelectorAll('.popup');
    if ($popups.length) {
        const popups = new Popups($popups, {});
        popups.init();
    }
}

// NOTE: initSwiper
{
    const $sliders = document.querySelectorAll('[data-slider]');

    if ($sliders.length) {
        for (let i = 0; i < $sliders.length; i++) {
            const slider = new Slider($sliders[i]);
            slider.init();
        }
    }
}

// Выбор страны
const worldMap = document.querySelector('.countries__holder');
const countries = document.querySelectorAll('.country');
const countryChoiseCircle = document.querySelector('.country-choise-circle');

const countriesGroupLink = document.querySelectorAll('.countries__group li > a');
const countriesNames = document.querySelectorAll('.countries__group li > a span');

function countryTake(i, noScrollToViewport = true) {
    for (let j = 0; j < countries.length; j++) {
        if (countriesNames[i].textContent.replace(/\s/g, '') == countries[j].id) {
            countryChoiseCirclePositionCount(countries[j].id, noScrollToViewport);
            countrySelect(countries[j].id);
        }
    }
}

function countryChoiseCirclePositionCount(countryTake, scrollToViewport = true) {
    const country = document.getElementById(countryTake);

    const worldMapX = worldMap.getBoundingClientRect().x;
    const worldMapY = worldMap.getBoundingClientRect().y;

    const countryX = country.getBoundingClientRect().x + country.getBoundingClientRect().width / 2;
    const countryY = country.getBoundingClientRect().y + country.getBoundingClientRect().height / 2;

    countryChoiseCircle.style.left = countryX - worldMapX + 'px';
    countryChoiseCircle.style.top = countryY - worldMapY + 'px';

    if (scrollToViewport) {
        const viewportCenter = worldMap.offsetTop + worldMap.offsetHeight / 2 - window.innerHeight / 2;
        scrollTo(document.documentElement, viewportCenter, 500);
    }
}

let countryPart = '';
function countrySelect(countryTake) {
    const country = document.getElementById(countryTake);
    if (country.tagName == 'g') {
        countryPart = country.querySelectorAll('path');
    }

    for (let j = 0; j < countries.length; j++) {
        countries[j].style.fill = '#D5DFEB';
    }
    for (let l = 0; l < countryPart.length; l++) {
        countryPart[l].style.fill = '#D5DFEB';
    }

    if (country.tagName == 'path') {
        country.style.fill = '#3d80f2';
    } else if (country.tagName == 'g') {
        for (let k = 0; k < countryPart.length; k++) {
            countryPart[k].style.fill = '#3d80f2';
        }
    }
}

function countryChoiseCircleAnimation() {
    countryChoiseCircle.classList.remove('country-choise-circle-animation');
    setTimeout(function () {
        countryChoiseCircle.classList.add('country-choise-circle-animation');
    }, 50);
}

function countriesGroupLinkClick(i) {
    countriesGroupLink[i].addEventListener('click', function (e) {
        e.preventDefault();
        countryTake(i);
        countryChoiseCircleAnimation();

        const startCount = scrollToViewport => {
            countryTake(i, scrollToViewport);
            countryChoiseCircleAnimation();
        };

        window.addEventListener('resize', startCount.bind(null, false));
    });
}

for (let i = 0; i < countriesGroupLink.length; i++) {
    countriesGroupLinkClick(i);
}

// NOTE: Scroll to form 'write-to-us'
{
    const $writeToUs = document.getElementById('write-to-us');
    const $form = document.getElementById('form');

    if ($writeToUs && $form) {
        document.body.addEventListener('click', e => {
            if (e.target.closest('.btn-anima')) {
                e.preventDefault();
                scrollTo(document.documentElement, $writeToUs.offsetTop + $form.offsetTop, 500);
            }
        });
    }
}

// NOTE: Observer elements
{
    const $elements = document.querySelectorAll('[data-el-anime]');

    if ($elements.length) {
        const detectViewport = new AnimeDetectViewport($elements);
        detectViewport.init();
    }
}

// NOTE: cursor custom
{
    const $holders = document.body.querySelectorAll('[data-cursor-holder]');

    if ($holders.length) {
        for (let i = 0; i < $holders.length; i++) {
            customCursor($holders[i]);
        }
    }
}

// NOTE: Accordion
{
    const $accordions = document.querySelectorAll('[data-accordion]');

    if ($accordions.length) {
        $accordions.forEach($accordion => {
            if ($accordion.getAttribute('data-accordion') === 'mq') {
                const mq = window.matchMedia('(max-width: 1199px)');
                const accordion = new Accordion($accordion);

                const checkMq = () => {
                    if (mq.matches) {
                        accordion.init();
                    } else {
                        accordion.destroy();
                    }
                };

                checkMq();
                mq.addListener(checkMq);
            } else {
                const accordion = new Accordion($accordion);
                accordion.init();
            }
        });
    }
}

// NOTE: Navbar
{
    const $navbar = document.querySelector('[data-navbar-menu]');
    const $toggler = document.querySelector('[data-navbar-toggler]');
    const $nav = document.querySelector('[data-nav]');

    if ($navbar && $toggler && $nav) {
        const mq = window.matchMedia('(max-width: 767px)');
        const navbarDesktop = new NavbarDesktop($navbar);
        const navbarMob = new NavbarMob($toggler, $nav);

        const checkMq = () => {
            if (mq.matches) {
                navbarDesktop.destroy();
                navbarMob.init();
            } else {
                navbarMob.destroy();
                navbarDesktop.init();
            }
        };

        checkMq();
        mq.addListener(checkMq);
    }
}

// NOTE: Send mail
{
    const $form = document.getElementById('form');
    if ($form) {
        sendMail($form);
    }
}

// NOTE: support
{
    const $link = document.querySelector('[data-link-support]');
    const $btns = document.querySelectorAll('[data-radio-btn]');
    const $title = document.querySelector('[data-title-support]');
    if ($link && $btns && $title) {
        $btns.forEach($btn => {
            $btn.addEventListener('click', (event) => {
                $btns.forEach(btn => btn.classList.remove('active'));
                const $label = event.target.closest('label');
                $label.classList.add('active');
                const href = $label.getAttribute('data-radio-btn');
                $link.href = href;
                const $linkText = $link.querySelector('span');
                if (href === 'https://telegram.me/paykassma_support') {
                    $linkText.innerText = 'paykassma_support';
                    $title.innerText = 'All Questions';
                } else {
                    $linkText.innerText = 'pkassmapaysup';
                    $title.innerText = 'Payment support';
                }
            });
        });
    }
}

//NOTE: Set current Year
{
    const $holderYear = document.querySelector('[data-current-year]');

    if ($holderYear) {
        setCurrentYear($holderYear);
    }
}

// Resize function
resizer({ emitter, ms: 300 });
